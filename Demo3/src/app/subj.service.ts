import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Subject } from './subject';

@Injectable({
  providedIn: 'root'
})
export class SubjService {

  constructor(private http:Http) { }

  getAll()
  {
  return this.http.get("http://localhost:49866/api/Subjects");
    
  }
  
  getSubj(id:number){
    return this.http.get("http://localhost:49866/api/Subjects/"+id);
  }
  
  addSubj(subj:Subject){
    return this.http.post("http://localhost:49866/api/Subjects",subj);
  }
  
  
  deleteSubj(id:number)
  {
    return this.http.delete("http://localhost:49866/api/Subjects/"+id);
  }
  
  editSubj(id:number,subj:Subject)
  {
    return this.http.put("http://localhost:49866/api/Subjects/"+id,subj);
  }

}
