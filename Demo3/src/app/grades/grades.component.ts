import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';
import { SubjService } from '../subj.service';
import { Student } from '../student';
import { Subject } from '../subject';
import { GradesService } from '../grades.service';
import { Grade } from '../grade';

@Component({
  selector: 'app-grades',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.css']
})
export class GradesComponent implements OnInit {

  constructor(private stdservice:StudentService,private subjservice:SubjService,private gradserv:GradesService) { }

  stdlist:Student[]=[];
  subjlist:Subject[]=[];
  ngOnInit() {

    this.stdservice.getAll().subscribe(
      d=>{this.stdlist=d.json();}
    );

    this.subjservice.getAll().subscribe(
      d=>{this.subjlist=d.json();}
    );
 this.gradserv.getAll().subscribe(
   d=>{
     this.gradeList=d.json();
   }
 );
    
  }//end of init functions
  selectedStd:number=1;
  selectedSubj:number[]=[];
gradeList:Grade[]=[];
  new_grade:Grade;
  submitGrades()
  {
      
         console.log("std id="+this.selectedStd);
          this.selectedSubj.forEach((i)=>{
            console.log("hello from inside subject array stdid="+this.selectedStd+"subjid="+i);
            this.new_grade=new Grade(0,this.selectedStd,i,0);
           this.gradserv.addgrade(this.new_grade).subscribe(
            d=>{
              this.gradeList.push(d.json());
            });
          });
alert("the subject are submitted successfully for the student");
       
  }//end of submit grades
     
  


}
