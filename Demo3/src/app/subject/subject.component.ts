import { Component, OnInit } from '@angular/core';
import { SubjService } from '../subj.service';
import { Subject } from '../subject';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css']
})
export class SubjectComponent implements OnInit {

  constructor(private subjservice:SubjService) { }

  ngOnInit() {
    this.subjservice.getAll().subscribe(
      d=>{this.subjlist=d.json();}
    );
  }

  nsubj:Subject=new Subject(0,'');
  subjlist:Subject[]=[];

subj:Subject=new Subject(1,'');

  showAllSubjects()
  {
    this.subjservice.getAll().subscribe(
      d=>{this.subjlist=d.json();}
    );
  }//end of function
//================================================
AddStd()
{
  this.subjservice.addSubj(this.nsubj).subscribe(
    d=>{
      this.subjlist.push(d.json());
    }
  );
}

subjDetails(id:number)
{
  this.subjservice.getSubj(id).subscribe(
    d=>{alert("============Subject Details=========="+
      "\nsubject id="+d.json().id+
    "\n subject name="+d.json().name)
  }
  );
}
subjDelete(hid:number)
{
  
  this.subjservice.deleteSubj(hid).subscribe(
d=>{
  this.subjlist.forEach((item,index,arr)=>{
    if(item.id==hid)
    {
      arr.splice(index,1);
    }
  });
  console.log(d.json().id+" delted")
}
  );
}//end of function

subjEdit(id:number)
{

  this.subjlist.forEach((s)=>{
    if(s.id==id)
    {
      this.subj.name=s.name;
      this.subj.id=s.id;
    }
  })

}


updateSubj()
{
  this.subjservice. editSubj(this.subj.id,this.subj).subscribe(
    a=>{
      
      this.subjlist.forEach((item)=>{
        if(item.id==this.subj.id)
        {
           item.name=this.subj.name;
         
        }
      });
      console.log("updated ok")}
  );
}


}
