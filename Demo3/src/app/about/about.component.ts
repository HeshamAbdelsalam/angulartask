import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';
import { Student } from '../student';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
  
})//
export class AboutComponent implements OnInit {

  constructor(private stdservice:StudentService) { }

   ngOnInit() {

  //   this.stdservice.getAll().subscribe(
  //     d=>{this.stdlist=d.json();}
    //);
   }


  stdlist:Student[]=[
    new Student(1,'',1),
    new Student(1,'',1),
    new Student(1,'',1)
  ];
  showAllStudents()
  {
    this.stdservice.getAll().subscribe(
      d=>{this.stdlist=d.json();}
    );
  }//end of function
nstd:Student=new Student(0,'',0);


std:Student=new Student(0,'',0);//for update resons

AddStd()
{
  this.stdservice.addStd(this.nstd).subscribe(
    d=>{
      this.stdlist.push(d.json());
    }
  );
}

stdDetails(id:number)
{
  this.stdservice.getStd(id).subscribe(
    d=>{alert("============Student Details=========="+
      "\nstudent id="+d.json().id+
    "\nstudent name="+d.json().name+
    "\nstudent age="+d.json().age)}
  );
}
delstd:Student=new Student(1,'',1);
stdDelete(hid:number)
{

  this.stdservice.deleteStd(hid).subscribe(
d=>{
 this.stdlist.forEach((item,index,arr)=>{
    if(item.id==hid)
    {
       arr.splice(index,1);
    }
  });
  console.log(d.json()+"delted")
}
  );
 
}//end of function


stdEdit(id:number)
{
   this.stdlist.forEach((s)=>{
     if(s.id==id)
     {
       this.std.name=s.name;
       this.std.age=s.age;
       this.std.id=s.id;
     }
   })
}

updaeStd(){

  this.stdservice.updateStd(this.std.id,this.std).subscribe(
    a=>{
      
      this.stdlist.forEach((item,index,arr)=>{
        if(item.id==this.std.id)
        {
           item.name=this.std.name;
           item.age=this.std.age;
        }
      });
      console.log("updated ok")}
  );
}//end of func



  }
