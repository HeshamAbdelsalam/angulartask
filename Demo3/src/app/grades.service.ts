import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Grade } from './grade';

@Injectable({
  providedIn: 'root'
})
export class GradesService {

  constructor(private http:Http) { }

  getAll()
  {
    console.log("enter service function");
  return this.http.get("http://localhost:49866/api/Grades"); 
  }

  getAllH(id:number)
  {
    console.log("enter service function");
  return this.http.get("http://localhost:49866/api/Grades/"+id); 
  }
  
  // getStd(id:number){
  //   return this.http.get("http://localhost:49866/api/Students/"+id);
  // }
  
  addgrade(gd:Grade){
    console.log("======== grades service==========running");
    return this.http.post("http://localhost:49866/api/Grades",gd);
    
  }
  

  updategrade(id:number, gd:Grade){
    console.log("===== update=== grades service==========running");
    return this.http.put("http://localhost:49866/api/Grades/"+id,gd);
    
  }


}
