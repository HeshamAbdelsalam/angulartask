import { TestBed } from '@angular/core/testing';

import { SubjService } from './subj.service';

describe('SubjService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubjService = TestBed.get(SubjService);
    expect(service).toBeTruthy();
  });
});
