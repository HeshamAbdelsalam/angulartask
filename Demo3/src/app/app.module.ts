import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {RouterModule,Routes} from'@angular/router'
import { LoginComponent } from './login/login.component';

import { AboutComponent } from './about/about.component';
import { MyLoggerService } from './my-logger.service';
import { StudentService } from './student.service';
import { SubjService } from './subj.service';
import { SubjectComponent } from './subject/subject.component';

import { StudentComponent } from './student/student.component';
import { GradesComponent } from './grades/grades.component';
import { GradesService } from './grades.service';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,

    AboutComponent,
    SubjectComponent,

    StudentComponent,
    GradesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    RouterModule.forRoot([
      {path:'std',component:AboutComponent},
      {path:'subj',component:SubjectComponent},
      {path:'grades',component:GradesComponent},
      {path:'stdent',component:StudentComponent}
    ])

  ],
  providers: [MyLoggerService,StudentService,SubjService,GradesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
