import { TestBed } from '@angular/core/testing';

import { StdSubjService } from './std-subj.service';

describe('StdSubjService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StdSubjService = TestBed.get(StdSubjService);
    expect(service).toBeTruthy();
  });
});
