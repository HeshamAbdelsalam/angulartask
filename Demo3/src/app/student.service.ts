import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Student } from './student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http:Http) { }

getAll()
{
return this.http.get("http://localhost:49866/api/Students");
  
}

getStd(id:number){
  return this.http.get("http://localhost:49866/api/Students/"+id);
}

addStd(std:Student){
  return this.http.post("http://localhost:49866/api/Students",std);
}


deleteStd(id:number)
{
  return this.http.delete("http://localhost:49866/api/Students/"+id);
}
updateStd(id:number,std:Student)
{
  return this.http.put("http://localhost:49866/api/Students/"+id,std);
}


}
