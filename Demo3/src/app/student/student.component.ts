import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';
import { SubjService } from '../subj.service';
import { GradesService } from '../grades.service';
import { Student } from '../student';
import { Grade } from '../grade';
import { GradeViewModel } from '../grade-view-model';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(private stdservice:StudentService,private subjservice:SubjService,private gradserv:GradesService) { }

  ngOnInit() {
    this.stdservice.getAll().subscribe(
      d=>{this.stdlist=d.json();}
    );
  }

  stdlist:Student[]=[];
  grdlist:GradeViewModel[]=[];

  selstd:number=1;

gradeupdList:Grade[]=[];

  manageGrade()
  {
     this.grdlist.length=0;
    //alert(this.selstd);
    console.log("enter component func");
    this.gradserv.getAllH(this.selstd).subscribe(d=>{
      this.grdlist=d.json();
      console.log(d.json());
   });

   this.gradeupdList.length=0;

  
   
  }

  newGrade:Grade;
  gradeValue:number;
  updateGrades(id:number)
  {
    console.log("begin update grades");
  
    this.grdlist.forEach((i)=>{
      if(i.id==id)
      {
      this.newGrade=new Grade(i.id,i.stdid,i.subjid,this.gradeValue);
      i.grade=this.gradeValue;
      }
    })
   
   
      this.gradserv.updategrade(id,this.newGrade).subscribe(d=>{

      })
    

    console.log(this.gradeupdList);
  }



}
