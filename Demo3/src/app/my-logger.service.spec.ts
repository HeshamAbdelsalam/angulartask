import { TestBed } from '@angular/core/testing';

import { MyLoggerService } from './my-logger.service';

describe('MyLoggerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyLoggerService = TestBed.get(MyLoggerService);
    expect(service).toBeTruthy();
  });
});
