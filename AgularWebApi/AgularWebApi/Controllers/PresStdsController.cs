﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AgularWebApi.Models;
using System.Web.Http.Cors;

namespace AgularWebApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200/", headers: "*", methods: "*")]
    public class PresStdsController : ApiController
    {
        private Model1 db = new Model1();

        // GET: api/PresStds
        public IQueryable<PresStd> Getprestds()
        {
            return db.prestds;
        }

        // GET: api/PresStds/5
        [ResponseType(typeof(PresStd))]
        public IHttpActionResult GetPresStd(int id)
        {
            PresStd presStd = db.prestds.Find(id);
            if (presStd == null)
            {
                return NotFound();
            }

            return Ok(presStd);
        }

        // PUT: api/PresStds/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPresStd(int id, PresStd presStd)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != presStd.id)
            {
                return BadRequest();
            }

            db.Entry(presStd).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PresStdExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PresStds
        [ResponseType(typeof(PresStd))]
        public IHttpActionResult PostPresStd(PresStd presStd)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.prestds.Add(presStd);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = presStd.id }, presStd);
        }

        // DELETE: api/PresStds/5
        [ResponseType(typeof(PresStd))]
        public IHttpActionResult DeletePresStd(int id)
        {
            PresStd presStd = db.prestds.Find(id);
            if (presStd == null)
            {
                return NotFound();
            }

            db.prestds.Remove(presStd);
            db.SaveChanges();

            return Ok(presStd);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PresStdExists(int id)
        {
            return db.prestds.Count(e => e.id == id) > 0;
        }
    }
}