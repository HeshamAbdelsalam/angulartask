﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AgularWebApi.Models
{
    public class Student
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string fname { get; set; }

        public string lname { get; set; }

        public int checkStd { get; set; } = 0;  //1=checked   0--not checked
      
        public int track_id { get; set; }


     

    }
}