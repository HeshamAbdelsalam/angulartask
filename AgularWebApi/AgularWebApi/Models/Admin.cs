﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AgularWebApi.Models
{
    public class Admin
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string fname { get; set; }

        public string lname { get; set; }

        public string mail { get; set; }

        public string password { get; set; }

        public byte[] pic { get; set; }

        public string phone { get; set; }

        public string address { get; set; }
    }
}