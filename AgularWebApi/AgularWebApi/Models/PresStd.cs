﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AgularWebApi.Models
{
    public class PresStd
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int std_id { get; set; }

        public DateTime stdDate { get; set; }

        public DateTime stdTime { get; set; }

    }
}