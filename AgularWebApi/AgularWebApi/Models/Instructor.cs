﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AgularWebApi.Models
{
    public class Instructor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string fname { get; set; }

        public string lname { get; set; }

        [ForeignKey("track")]
        public int track_id { get; set; }

        public Track track { get; set; }
    }
}