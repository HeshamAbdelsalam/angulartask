namespace AgularWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class h23 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        fname = c.String(),
                        lname = c.String(),
                        mail = c.String(),
                        password = c.String(),
                        pic = c.Binary(),
                        phone = c.String(),
                        address = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        fname = c.String(),
                        lname = c.String(),
                        mail = c.String(),
                        password = c.String(),
                        pic = c.Binary(),
                        phone = c.String(),
                        address = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Instructors",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        fname = c.String(),
                        lname = c.String(),
                        track_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Tracks", t => t.track_id, cascadeDelete: true)
                .Index(t => t.track_id);
            
            CreateTable(
                "dbo.Tracks",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        desc = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PresStds",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        std_id = c.Int(nullable: false),
                        stdDate = c.DateTime(nullable: false),
                        stdTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        fname = c.String(),
                        lname = c.String(),
                        checkStd = c.Int(nullable: false),
                        track_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Instructors", "track_id", "dbo.Tracks");
            DropIndex("dbo.Instructors", new[] { "track_id" });
            DropTable("dbo.Students");
            DropTable("dbo.PresStds");
            DropTable("dbo.Tracks");
            DropTable("dbo.Instructors");
            DropTable("dbo.Employees");
            DropTable("dbo.Admins");
        }
    }
}
